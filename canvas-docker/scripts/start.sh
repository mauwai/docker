#!/bin/sh
bundle exec rake brand_configs:generate_and_upload_all
bundle exec rake db:initial_setup
bundle exec puma -p $CANVAS_LMS_PORT -w $CANVAS_LMS_WORKERCOUNT -e $RAILS_ENV